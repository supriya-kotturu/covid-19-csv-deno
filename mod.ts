import { join } from "./lib/deps.ts";
import { BufReader } from "./lib/deps.ts";
import { flagParse } from "./lib/deps.ts";
import { encodingParse } from "./lib/deps.ts";
import { _ } from "./lib/deps.ts";
import { Lib as Helpers } from "./lib/helpers.ts";

// returns Countries with Cummulative Cases > 250K and Cumulative Deaths > 25k

interface Args {
  [key: string]: number;
}
interface CovidData {
  [key: string]: string;
}

// get the arguments
const args = flagParse(Deno.args, {
  default: {
    country: "India",
    days: 5,
    cases: 0,
    deaths: 0,
    "new-cases": 0,
    "new-deaths": 0,
  },
});

const helpers = new Helpers();

async function loadCovidWorldData(
  args: Args,
  country: string,
): Promise<Array<string>> {
  //  get the path of the .csv
  const path = join("./resources", "WHO-COVID-19-global-data.csv");

  //  read the file
  const file = await Deno.open(path);
  const bufReader = new BufReader(file);
  const data = await encodingParse(bufReader, {
    header: true,
  });

  Deno.close(file.rid);

  const countriesData = (data as Array<CovidData>).filter((element) => {
    //   console.log("ele: ", element);
    //   console.log(Object.keys(element));
    const cumulativeDeaths = parseInt(element[" Cumulative_cases"]);
    const newDeaths = parseInt(element[" New_deaths"]);
    const cumulativeCases = parseInt(element[" Cumulative_cases"]);
    const newCases = parseInt(element[" New_cases"]);

    // returns all the  in the file if args aren't provided
    return cumulativeCases > args.cases && cumulativeDeaths > args.deaths &&
      newCases > args["new-cases"] && newDeaths > args["new-deaths"];
  });

  const sortedByDeaths = helpers.sortCountries(countriesData).map((element) => {
    return _.pick(element, [
      "Date_reported",
      " Country",
      " New_cases",
      " Cumulative_cases",
      " New_deaths",
      " Cumulative_deaths",
    ]);
  });
  console.log(
    `\nCOVID statistics of ${country} in the past ${args.days} days\n`,
  );

  helpers.showTopFiveData(sortedByDeaths, country, args.days);
  return helpers.getCountriesList(sortedByDeaths);
}

function getCountries(country: string[], found: boolean = false): void {
  if (found) {
    console.log(
      "\nAccording to WHO, A list of TOP 10 countries affected by COVID-19",
    );
    args.cases > 0 ? console.log(`cases > ${args.cases}`) : false;
    args.deaths > 0 ? console.log(`deaths > ${args.deaths}`) : false;
    args["new-cases"] > 0
      ? console.log(`new Cases > ${args["new-cases"]}`)
      : false;
    args["new-deaths"] > 0
      ? console.log(`new Deaths > ${args["new-deaths"]}`)
      : false;

    console.log();
    let count = countries.length < 10 ? countries.length : 10;
    for (let i = 0; i < count; i++) {
      console.log(countries[i]);
    }
  } else {
    console.log(
      "\nNO COUNTRIES ARE FOUND with COVID-19 statistics",
    );
    args.cases > 0 ? console.log(`cases > ${args.cases}`) : false;
    args.deaths > 0 ? console.log(`deaths > ${args.deaths}`) : false;
    args["new-cases"] > 0
      ? console.log(`new Cases > ${args["new-cases"]}`)
      : false;
    args["new-deaths"] > 0
      ? console.log(`new Deaths > ${args["new-deaths"]}`)
      : false;
  }
}

let countries: string[] = [];
let c = args.country;
// let country = c.replace(/-/g, " ")

switch (args.country.toLowerCase()) {
  case "usa":
  case "america":
  case "united-states":
    c = "United States of America";
    break;
  case "uk":
    c = "The United Kingdom";
    break;
  case "iran":
    c = "Iran (Islamic Republic of)";
    break;
  case "russia":
    c = "Russian Federation";
    break;
  case "s-africa":
    c = "South Africa";
    break;
  case "saudi-arabia":
    c = "Saudi Arabia";
    break;
  case "venezuela":
    c = "Venezuela (Bolivarian Republic of)";
    break;
  case "kosovo":
    c = "Kosovo[1]";
    break;
  case "congo":
    c = "Democratic Republic of the Congo";
    break;
  case "korea":
    c = "Republic of Korea";
    break;
  case "arab":
    c = "United Arab Emirates";
    break;
  case "n-macedonia":
  case "noth-macedonia":
    c = "North Macedonia";
    break;
  case "democratic-republic-of-the-congo":
  case "congo":
    c = "Democratic Republic of the Congo";
    break;
  case "puerto-rico":
    c = "Puerto Rico";
    break;
  case "palestine":
    c = "occupied Palestinian territory, including east Jerusalem";
    break;
  case "central-african-republic":
  case "entral-africa":
    c = "Central African Republic";
    break;

  default:
    c = args.country;
}

// console.log(c);
countries = await loadCovidWorldData(args, c);
countries.length > 0
  ? getCountries(countries, true)
  : getCountries(countries, false);

// console.log(args);
