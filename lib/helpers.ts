interface CovidData {
  [key: string]: string;
}

export class Lib {
  sortCountries(
    elements: Array<CovidData>,
  ): Array<CovidData> {
    let sortedElements: Array<CovidData> = [];

    sortedElements = elements.sort((ele1, ele2) => {
      return parseInt(ele2[" Cumulative_deaths"]) -
        parseInt(ele1[" Cumulative_deaths"]);
    });
    return sortedElements;
  }

  getCountriesList(elements: Array<CovidData>): Array<string> {
    let countriesList: string[] = [];

    elements.forEach((element) => {
      countriesList.push(element[" Country"]);
    });
    const uniqueCountryList = [...new Set(countriesList)];
    return uniqueCountryList;
  }

  showTopFiveData(
    elements: Array<CovidData>,
    country: string = "India",
    days: number = 5,
  ): void {
    country = country.replace(/^\w/, (c) => c.toUpperCase());
    console.log(
      `REPORTED DATE\t| COUNTRY\t| NEW DEATHS\t| NEW CASES `,
    );
    let count = 0;
    for (let i = 0; i < elements.length && count < days; i++) {
      if (`${elements[i][" Country"]}` == country) {
        console.log(
          `${elements[i]["Date_reported"]}\t|${elements[i][" Country"]}\t\t|${
            elements[i][" New_deaths"]
          }\t\t| ${elements[i][" New_cases"]}\t\t`,
        );
        count++;
      }
    }
  }

  displayArray(elements: Array<string>): void {
    let arryLen = elements.length < 5 ? elements.length : 5;
    for (let i = 0; i < arryLen; i++) {
      console.log(elements[i]);
    }
  }
}

// const handler = new lib();
// let obj: Array<CovidData> = [{
//   "Date_reported": "11-06-20",
//   " Country_code": "BR",
//   " Country": "Brazil",
//   " WHO_region": "AMRO",
//   " New_cases": "32091",
//   " Cumulative_cases": "739503",
//   " New_deaths": "1272",
//   " Cumulative_deaths": "38406",
// }, {
// ]
