# Covid-19 csv deno

- Lists the recent cases and death counts of a specific country in the recent days
- Lists the top 10 countries affected by COVID-19

## Install Deno 

Shell (Mac, Linux):

`curl -fsSL https://deno.land/x/install/install.sh | sh`

PowerShell (Windows): 

`iwr https://deno.land/x/install/install.ps1 -useb | iex`

Homebrew (Mac): 

`brew install deno`

Find other ways to install Deno at [Deno.land](https://deno.land/#installation)

## Getting Started

Try running the program with default arguments:

`deno run --allow-read mod.ts`

Or run it by specifying values for Country and Days as follows:

`deno run --allow-read mod.ts --country russia --days 3`

## Resources

The .csv file is referred from [WHO Coronavirus Disease (COVID-19) Dashboard](https://covid19.who.int/)
