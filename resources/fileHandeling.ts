import { join } from "../lib/deps.ts";
import { BufReader } from "../lib/deps.ts";
import { encodingParse } from "../lib/deps.ts";

let fileData: string,
  endodedFileData: Uint8Array,
  decodedFileData: string;

const path = join(".", "sampleFile.txt");

console.log(path, " ");
// readTextFile
fileData = await Deno.readTextFile(path);
console.log(`\nFile read using Deno.readTextFile() :\n${fileData}`);

fileData = Deno.readTextFileSync(path);
console.log(`\nFile read using Deno.readTextFileSync() :\n${fileData}`);

// readFile
//  - need to use DECODER
const decoder = new TextDecoder("utf-8");

endodedFileData = await Deno.readFile(path);
decodedFileData = decoder.decode(endodedFileData);
console.log(`\nFile read using Deno.readFile() :\n${decodedFileData}`);

endodedFileData = Deno.readFileSync(path);
decodedFileData = decoder.decode(endodedFileData);
console.log(`\nFile read using Deno.readFileSync() :\n${decodedFileData}`);

// open
// -  requires decoder to decode the utf-8 encoding
const file = await Deno.open(path);
const bufReader = new BufReader(file);
const data = await encodingParse(bufReader);
console.log(`\nFile read using Deno.open() :\n${data}`);
Deno.close(file.rid);

// readDir
//  - reads the directory and returns each Entry as an object
console.log(`\nFiles in the current directory using Deno.readDir():`);
for await (const dirEntry of Deno.readDir(Deno.cwd())) {
  console.log(dirEntry.name);
}

console.log(`\nFiles in the current directory using Deno.readDirSync():`);
for (const dirEntry of Deno.readDirSync(Deno.cwd())) {
  // console.log(dirEntry);
  console.log(dirEntry.name);
}
